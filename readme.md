# DQN approach for building an autonomous system that learns how to play atari style video games

Implementation of the dqn algorithm to play atari games from open ai gym.

This algorithm is not generalized for breakout and can work with any environment from open ai gym. The algorithm formulates the general Markov Decision Process (MDP) and passes the input image to CNN as reinforcement learning is implemented.

There are some issues that I intend to fix later, including the use of epsilon greedy approach instead of softmax activation function, and the use of prioritized experience replay.

The algorithm was only trained for 50 episodes, and while I seem to have achieved miniscule amount of learning, I need access to a gpu to run the code for more iterations to achieve significant learning.

To run the code, open up the terminal and change the currect directory to CS_SeniorThesis.
Once inside the directory, type ```*python ai.py*.```

It is important to remember that the code was written in python 2. So, make sure that your machine has python 2 installed, and if ```python ai.py``` defaults to python 3, then your machine probably doesn't have python 2 installed.

To download python 2, visit the following link:

- [Link to python 2 download](https://www.python.org/downloads/)

To download other necessary packages to run the code, visit the following link:

- [Link to openai gym documentation](https://gym.openai.com/docs/)
- [Link to sciPy installation](https://scipy.org/install.html)
- [Link to numpy installation](https://scipy.org/install.html)
- [Link to pyTorch installation](http://pytorch.org/)
