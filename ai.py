# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 19:58:48 2018

@author: haques18
"""
#Alright... this is it. I got this.
#I'm gonna charge myself with Red Bull and pull off this monumental feat before demo day
#Please and Thank you.

#Switching from Keras and Theano to PyTorch because the implementation with PyTorch is faster


# Importing the libraries
import numpy as np
import random
import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

from collections import namedtuple

import gym
from gym.wrappers import SkipWrapper as sw  # to import all the tools and environment of gym
import image_preprocessor

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))

class CNN(nn.Module):
    
    #Building a CNN with 3 convolutional layer and 1 hidden layer
    #input: number_actions - number of output neurones there should be in the cnn
    def __init__(self, numberOfActions):
        #activate the inheritence of the super function of nn
        super(CNN, self).__init__()
        #Applies convolution to input images to get the first convolutional layer
        self.convolution1 = nn.Conv2d(in_channels = 1, out_channels = 16, kernel_size = 5)
        #Take the first convolutional layer as input
        #Applies convolution to get the second convolutional layer
        self.convolution2 = nn.Conv2d(in_channels = 16, out_channels = 32, kernel_size = 5)
        #Take the second convolutional layer as input
        #Applies convolution to get the third convolutional layer
        self.convolution3 = nn.Conv2d(in_channels = 32, out_channels = 64, kernel_size = 5)
        #Conection from the third convolutionl layer to the hidden layer
        self.fc1 = nn.Linear(in_features = self.countNeurons((1, 80, 80)), out_features = 40)
        #Conection from the hidden layer to the output layer 
        self.fc2 = nn.Linear(in_features = 40, out_features = numberOfActions)
        
    #input: image_dim - size of the input image
    #output: size of the flattened vector after applying the 3 convolutions
    def countNeurons(self, image_dim):
    	#Creates a fake image with the same dimensions as the input image
    	#* converts a tuple to a list of arguments for a function
        x = Variable(torch.rand(1, *image_dim))
        #Apply convolution to the images
        #Followed by max_pooling
        #Followed by activation of the neurones with the rectifier activation function
        x = F.relu(F.max_pool2d(self.convolution1(x), 3, 2))
        x = F.relu(F.max_pool2d(self.convolution2(x), 3, 2))
        x = F.relu(F.max_pool2d(self.convolution3(x), 3, 2))
        #Flattens the output from the third convolutional layer and calculates its size
        return x.data.view(1, -1).size(1)
        
    #propagates the signal in all the layers of the neural network
    #input: input image
    def forward(self, x):
        #Apply convolution to the image
        x = F.relu(F.max_pool2d(self.convolution1(x), 3, 2))
        x = F.relu(F.max_pool2d(self.convolution2(x), 3, 2))
        x = F.relu(F.max_pool2d(self.convolution3(x), 3, 2))
        #Flatten the output of the third convolutional layer
        x = x.view(x.size(0), -1)
        #Pass the flattened image vector to the hidden layer
        x = F.relu(self.fc1(x))
        #Pass the result of applying the hidden layer to the output neurons
        x = self.fc2(x)
        return x


# Making the AI
class SoftmaxBody(nn.Module):
    
    def __init__(self, T):
        super(SoftmaxBody, self).__init__()
        #T ensures that the action with the highest probability is selected
        self.T = T
        
    #Forwards the output signal from the brain to the body
    #input: output signal of the brain (output of the neural network)
    def forward(self, outputs):
    	#Get the probability distribution of the possible actions
        probs = F.softmax(outputs * self.T)
        #Sample the final action to play from probs
        actions = probs.multinomial()
        return actions
        
class AI:

    def __init__(self, brain, body):
        self.brain = brain
        self.body = body

    #Propagates the signal from the brain getting the image to the ai playing the action
    #Input: input images
    def __call__(self, inputs):
        #converting image in the right format
        input = Variable(torch.from_numpy(np.array(inputs, dtype = np.float32)))
        #Propagates the images into the brain of the ai to return the Q-values
        output = self.brain(input)
        #Propagates the output of the ai into the body of the ai
        actions = self.body(output)
        #returns the actions of the ai in the numpy array format 
        return actions.data.numpy()

# Part 2 - Training the AI with Deep Convolutional Q-Learning

# Implementing Experience Replay

class ReplayMemory(object):

    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, *args):
        """Saves a transition."""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)

    
# Getting the Breakout environment
env = gym.make('Breakout-v0')
breakout_env = image_preprocessor.PreprocessImage(env, width = 80, height = 80, grayscale = True)
breakout_env = gym.wrappers.Monitor(breakout_env, "videos", force = True)
number_actions = breakout_env.action_space.n

# Building an AI
cnn = CNN(number_actions)
softmax_body = SoftmaxBody(T = 1.0)
ai = AI(brain = cnn, body = softmax_body)
target_ai = AI(brain = cnn, body = softmax_body)
target_ai.brain.load_state_dict(ai.brain.state_dict())


memory = ReplayMemory(100000)
optimizer = optim.RMSprop(ai.brain.parameters())
BATCH_SIZE = 128
GAMMA = 0.999
TARGET_UPDATE = 10

def optimize_network():
    if len(memory) < BATCH_SIZE:
        return
    transitions = memory.sample(BATCH_SIZE)
    
    batch = Transition(*zip(*transitions))

    # Compute a mask of non-final states and concatenate the batch elements
    non_final_mask = torch.ByteTensor(tuple(map(lambda s: s is not None,
                                          batch.next_state)))
    next_state_tensor = torch.FloatTensor([np.ndarray.tolist(s) for s in list(batch.next_state) if s is not None])
    state_tensor = torch.FloatTensor([np.ndarray.tolist(s) for s in batch.state])
    action_tensor = torch.FloatTensor(list(batch.action)).unsqueeze(0)
    reward_tensor = batch.reward
    non_final_next_states = Variable(torch.cat(next_state_tensor), volatile=True)
    state_batch = Variable(torch.cat(state_tensor))
    action_batch = Variable(torch.cat(action_tensor))
    reward_batch = Variable(torch.cat(reward_tensor))

    # Compute Q(s_t, a) - the model computes Q(s_t), then we select the
    # columns of actions taken
    state_action_values = ai.brain(state_batch.unsqueeze(1))
    state_action_values = state_action_values.gather(1, action_batch.long().unsqueeze(1))

    # Compute V(s_{t+1}) for all next states.
    next_state_values = Variable(torch.zeros(BATCH_SIZE).type(torch.Tensor))
    next_state_values[non_final_mask] = target_ai.brain(non_final_next_states.unsqueeze(1)).max(1)[0]
    # Compute the expected Q values
    expected_state_action_values = (next_state_values * GAMMA) + reward_batch
    # Undo volatility (which was used to prevent unnecessary gradients)
    expected_state_action_values = Variable(expected_state_action_values.data)

    # Compute Huber loss
    loss = F.smooth_l1_loss(state_action_values, expected_state_action_values)

    # Optimize the model
    optimizer.zero_grad()
    loss.backward()
    for param in ai.brain.parameters():
        param.grad.data.clamp_(-1, 1)
    optimizer.step()


num_episodes = 50
for i_episode in range(num_episodes):
    # Initialize the environment and state
    print("Episode #:", i_episode + 1)
    last_screen = breakout_env.reset()
    current_screen = breakout_env.reset()
    state = current_screen - last_screen
    done = False
    while not done:
        # Select and perform an action
        action = ai(np.array([state]))[0][0]
        next_state, reward, done, _ = breakout_env.step(action)
        reward = torch.FloatTensor([reward])

        # Observe new state
        last_screen = current_screen
        current_screen = next_state

        if not done:
            next_state = current_screen - last_screen
            
        # Store the transition in memory
        memory.push(state, action, next_state, reward)

        # Move to the next state
        state = next_state

        # Perform one step of the optimization (on the target network)
        optimize_network()

    # Update the target network
    if i_episode % TARGET_UPDATE == 0:
        target_ai.brain.load_state_dict(ai.brain.state_dict())


print('Complete')
breakout_env.render(close=True)
breakout_env.close()